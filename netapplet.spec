Name:		netapplet
Summary:	Network switching and control applet.
Version:	0.90.0
Release:	7
License:	GPL
Group:		Applications/System
BuildRoot:	/var/tmp/%{name}-%{version}-%{release}-root

URL:		http://www.gnome.org
Source:		%{name}-%{version}.tar.gz
BuildRequires:	gtk2-devel >= 2.4.0
BuildRequires:	glib2-devel >= 2.4.0
BuildRequires:	wireless-tools >= 27pre12
Prereq:		wireless-tools >= 27pre12
Prereq:		gtk2 >= 2.4.0

%description
Network switching and control applet.

%prep
%setup -q

%build
./configure --prefix=/opt/gnome --sysconfdir=/etc/opt/gnome --mandir=/opt/gnome/man --infodir=/opt/gnome/info --localstatedir=/var --datadir=/opt/gnome/share
make

%install
mkdir -p $RPM_BUILD_ROOT/opt/gnome/bin/
mkdir -p $RPM_BUILD_ROOT/opt/gnome/share/netapplet/
mkdir -p $RPM_BUILD_ROOT/opt/gnome/share/icons/gnome/16x16/
mkdir -p $RPM_BUILD_ROOT/opt/gnome/share/icons/gnome/24x24/
mkdir -p $RPM_BUILD_ROOT/etc/init.d/
cp src/netdaemon $RPM_BUILD_ROOT/opt/gnome/bin/
cp src/netapplet $RPM_BUILD_ROOT/opt/gnome/bin/
cp src/netapplet.glade $RPM_BUILD_ROOT/opt/gnome/share/netapplet/
cp icons/16x16/*.png $RPM_BUILD_ROOT/opt/gnome/share/icons/gnome/16x16/
cp icons/24x24/*.png $RPM_BUILD_ROOT/opt/gnome/share/icons/gnome/24x24/
cp netdaemon $RPM_BUILD_ROOT/etc/init.d/

%post
/sbin/chkconfig --add netdaemon
/etc/init.d/netdaemon restart > /dev/null 2>&1

%preun
if [ $1 = 0 ]; then
	/etc/init.d/netdaemon stop > /dev/null 2>&1
	/sbin/chkconfig --del netdaemon
fi

%postun
if [ "$1" -ge "1" ]; then
	/etc/init.d/netdaemon restart > /dev/null 2>&1
fi

%files
%defattr(-,root,root)
%dir /opt/gnome/share/netapplet
/opt/gnome/share/netapplet/netapplet.glade
/opt/gnome/bin/netapplet
/opt/gnome/bin/netdaemon
/opt/gnome/share/icons/gnome/16x16/gnome-dev-ethernet.png
/opt/gnome/share/icons/gnome/16x16/gnome-dev-modem.png
/opt/gnome/share/icons/gnome/16x16/gnome-dev-wavelan-encrypted.png
/opt/gnome/share/icons/gnome/16x16/gnome-dev-wavelan.png
/opt/gnome/share/icons/gnome/24x24/gnome-dev-ethernet.png
/opt/gnome/share/icons/gnome/24x24/gnome-dev-modem.png
/opt/gnome/share/icons/gnome/24x24/gnome-dev-wavelan-encrypted.png
/opt/gnome/share/icons/gnome/24x24/gnome-dev-wavelan.png
/etc/init.d/netdaemon

%changelog
* Thu Jul 08 2004 Robert Love <rml@ximian.com>
- initial package 
