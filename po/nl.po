# Dutch translation of netapplet.
# Copyright (C) 2004, 2005 Free Software Foundation, Inc.
# This file is distributed under the same license as the netapplet package.
# Taco Witte <tcwitte@cs.uu.nl>, 2004, 2005.
# Tino Meinen <a.t.meinen@chello.nl>, 2005.
#
msgid ""
msgstr ""
"Project-Id-Version: netapplet HEAD\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2005-08-30 20:31+0000\n"
"PO-Revision-Date: 2005-08-31 01:20+0200\n"
"Last-Translator: Tino Meinen <a.t.meinen@chello.nl>\n"
"Language-Team: Dutch <vertaling@vrijschrift.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../src/eggtrayicon.c:121
msgid "Orientation"
msgstr "Oriëntatie"

#: ../src/eggtrayicon.c:122
msgid "The orientation of the tray."
msgstr "De oriëntatie van het vak."

#: ../src/netapplet.c:152
msgid "Disconnected"
msgstr "Niet verbonden"

#: ../src/netapplet.c:154
msgid "Ethernet connection"
msgstr "Ethernetverbinding"

#: ../src/netapplet.c:156
msgid "Dial-up connection"
msgstr "Inbelverbinding"

#: ../src/netapplet.c:316 ../src/netapplet.c:337
msgid ""
"<span weight=\"bold\" size=\"larger\">Network switching is currently "
"unavailable</span>\n"
"\n"
"The \"netdaemon\" service is not running"
msgstr ""
"<span weight=\"bold\" size=\"larger\">Netwerkschakelen is op dit moment niet "
"beschikbaar</span>\n"
"\n"
"De \"netdaemon\" dienst draait niet"

#: ../src/netapplet.c:384
msgid ""
"<span weight=\"bold\" size=\"larger\">Do you want to disconnect all network "
"connections?</span>\n"
"\n"
"Applications which use the network, such as web browsers and email programs, "
"will likely stop working while you are disconnected."
msgstr ""
"<span weight=\"bold\" size=\"larger\">Wilt u alle netwerkverbindingen "
"verbreken?</span>\n"
"\n"
"Toepassingen die het netwerk gebruiken, zoals webbrowsers en e-mail "
"programma's, zullen waarschijnlijk stoppen te werken als u niet verbonden "
"bent."

#: ../src/netapplet.c:416
#, c-format
msgid "Error trying to set default keyring: %d"
msgstr "Fout bij proberen standaard sleutelbos in te stellen: %d"

#: ../src/netapplet.c:435
#, c-format
msgid "Error trying to create keyring: %d"
msgstr "Fout bij proberen sleutelbos te maken: %d"

#: ../src/netapplet.c:452
#, c-format
msgid "Error trying to get default keyring: %d"
msgstr "Fout bij proberen standaard sleutelbos op te vragen: %d"

#: ../src/netapplet.c:504
#, c-format
msgid "Unable to save to keyring!  Err: %d"
msgstr "Kan niet opslaan naar sleutelbos!  Fout: %d"

#: ../src/netapplet.c:521
#, c-format
msgid "Password for network \"%s\""
msgstr "Wachtwoord voor netwerk \"%s\""

#: ../src/netapplet.c:611
msgid ""
"<span weight=\"bold\" size=\"larger\">Invalid Encryption Key: </span>\n"
"\n"
"Key contains illegal characters!"
msgstr ""
"<span weight=\"bold\" size=\"larger\">Ongeldige encryptiesleutel: </span>\n"
"\n"
"Sleutel bevat ongeldige tekens!"

#: ../src/netapplet.c:736
msgid ""
"<span weight=\"bold\" size=\"larger\">Invalid ESSID: </span>\n"
"\n"
"ESSID is blank or contains illegal characters!"
msgstr ""
"<span weight=\"bold\" size=\"larger\">Ongeldige ESSID: </span>\n"
"\n"
"ESSID is leeg of bevat ongeldige tekens!"

#: ../src/netapplet.c:752
msgid ""
"<span weight=\"bold\" size=\"larger\">Invalid Encryption Key:</span>\n"
"\n"
"Key contains illegal characters!"
msgstr ""
"<span weight=\"bold\" size=\"larger\">Ongeldige Encryptiesleutel:</span>\n"
"\n"
"Sleutel bevat ongeldige tekens!"

#: ../src/netapplet.c:806
msgid ""
"<span weight=\"bold\" size=\"larger\">Error displaying connection "
"information: </span>\n"
"\n"
"No active connection!"
msgstr ""
"<span weight=\"bold\" size=\"larger\">Fout bij weergeven "
"verbindingsinformatie: </span>\n"
"\n"
"Er is geen actieve verbinding!"

#: ../src/netapplet.c:822
msgid ""
"<span weight=\"bold\" size=\"larger\">Error displaying connection "
"information: </span>\n"
"\n"
"Unable to open socket!"
msgstr ""
"<span weight=\"bold\" size=\"larger\">Fout bij weergeven "
"verbindingsinformatie: </span>\n"
"\n"
"Kan socket niet openen!"

#: ../src/netapplet.c:844
msgid ""
"<span weight=\"bold\" size=\"larger\">Error displaying information: </span>\n"
"\n"
"SIOCGIFFLAGS failed on socket!"
msgstr ""
"<span weight=\"bold\" size=\"larger\">Fout bij weergeven informatie: </"
"span>\n"
"\n"
"SIOCGIFFLAGS mislukt op socket!"

#: ../src/netapplet.c:988
#, c-format
msgid ""
"<span weight=\"bold\" size=\"larger\">Network configuration could not be "
"run</span>\n"
"\n"
"%s"
msgstr ""
"<span weight=\"bold\" size=\"larger\">Netwerk configuratie kon niet worden "
"uitgevoerd</span>\n"
"\n"
"%s"

#: ../src/netapplet.c:1073
msgid "Network Connections"
msgstr "Netwerkverbindingen"

#: ../src/netapplet.c:1076
msgid "No network connections available"
msgstr "Er zijn geen netwerkverbindingen beschikbaar"

#: ../src/netapplet.c:1088
#, c-format
msgid "%s: %s (active)"
msgstr "%s: %s (actief)"

#: ../src/netapplet.c:1133
msgid "Wireless Networks"
msgstr "Draadloze netwerken"

#: ../src/netapplet.c:1136
msgid "Wireless disabled"
msgstr "Draadloos uitgeschakeld"

#: ../src/netapplet.c:1139
msgid "No wireless networks available"
msgstr "Er zijn geen draadloze netwerken beschikbaar"

#: ../src/netapplet.c:1150
#, c-format
msgid "%s (active)"
msgstr "%s (actief)"

#: ../src/netapplet.c:1190
msgid "Connection _Information"
msgstr "Verbindings_informatie"

#: ../src/netapplet.c:1200
msgid "_Configure Network Settings"
msgstr "Netwerkinstellingen _instellen"

#: ../src/netapplet.c:1211
msgid "_Remove From Panel"
msgstr "Ve_rwijderen van paneel"

#: ../src/netapplet.c:1669
msgid ""
"<span weight=\"bold\" size=\"larger\">Network switching is currently "
"unavailable</span>\n"
"\n"
"You do not have the permissions to change network settings"
msgstr ""
"<span weight=\"bold\" size=\"larger\">Netwerkschakelen is op dit moment niet "
"beschikbaar</span>\n"
"\n"
"U heeft geen toegangsrechten om netwerkinstellingen te wijzigen"

#: ../src/netapplet.glade.h:1
msgid "*"
msgstr "*"

#: ../src/netapplet.glade.h:2
msgid ""
"<span weight=\"bold\" size=\"larger\">Active Connection Information</span>"
msgstr ""
"<span weight=\"bold\" size=\"larger\">Actieve verbindingsinformatie</span>"

#: ../src/netapplet.glade.h:3
msgid "Add to Keyring"
msgstr "Toevoegen aan sleutelbos"

#: ../src/netapplet.glade.h:4
msgid "Broadcast Address:"
msgstr "Broadcast-adres:"

#: ../src/netapplet.glade.h:5
msgid "Connection Information"
msgstr "Verbindingsinformatie"

#: ../src/netapplet.glade.h:6
msgid "Destination Address:"
msgstr "Doeladres:"

#: ../src/netapplet.glade.h:7
msgid "ESSID:"
msgstr "ESSID:"

#: ../src/netapplet.glade.h:8
msgid "Encryption Key:"
msgstr "Coderingssleutel:"

#: ../src/netapplet.glade.h:9
msgid "Hardware Address:"
msgstr "Hardware-adres:"

#: ../src/netapplet.glade.h:10
msgid "IP Address:"
msgstr "IP-adres:"

#: ../src/netapplet.glade.h:11
msgid "Interface:"
msgstr "Interface:"

#: ../src/netapplet.glade.h:12
msgid "Show Encryption Key"
msgstr "Coderingssleutel weergeven"

#: ../src/netapplet.glade.h:13
msgid "Specify an ESSID"
msgstr "Een ESSID opgeven"

#: ../src/netapplet.glade.h:14
msgid "Specify the key"
msgstr "Geef de sleutel op"

#: ../src/netapplet.glade.h:15
msgid "Subnet Mask:"
msgstr "Subnet masker:"

#: ../src/netapplet.glade.h:16
msgid "Type:"
msgstr "Soort:"

#: ../share/share.glade.h:1
msgid "<b>Internet Sharing</b>"
msgstr "<b>Internetverbinding delen</b>"

#: ../share/share.glade.h:2
msgid "Share your connection from:"
msgstr "Uw verbinding delen van:"

# Begin/beginnen/starten
#: ../share/share.glade.h:3
msgid "Start"
msgstr "Starten"

#: ../share/share.glade.h:4
msgid "To computers using:"
msgstr "Naar computers met:"

#~ msgid "Wireless connection"
#~ msgstr "Draadloze verbinding"

#~ msgid "Wireless connection (secure)"
#~ msgstr "Draadloze verbinding (veilig)"
