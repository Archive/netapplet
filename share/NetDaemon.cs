//
//
using System;
using System.IO;
using System.Net.Sockets;
using System.Text;
using Mono.Posix;
using System.Threading;

public class NetDaemonConnection {
	StreamReader reader;
	StreamWriter writer;

	public NetDaemonConnection ()
	{
		UnixEndPoint ep = new UnixEndPoint ("/var/run/netapplet.socket");
		Socket s = new Socket (AddressFamily.Unix, SocketType.Stream, ProtocolType.IP);
		s.Connect (ep);
		
		NetworkStream ns = new NetworkStream (s);
		writer = new StreamWriter (ns, Encoding.ASCII);
		reader = new StreamReader (ns, Encoding.ASCII);
	}

	public StreamReader Reader {
		get { return reader; }
	}

	public StreamWriter Writer {
		get { return writer; }
	}
}

public delegate void NetCommand (string command, string [] args);
	
public class NetDaemon {
	NetDaemonConnection ndc;

	string last_interfaces;
	
	public NetDaemon ()
	{
		ndc = new NetDaemonConnection ();
	}

	public void Run ()
	{
		ThreadPool.QueueUserWorkItem (Work);
	}

	void Work (object data)
	{
		try {
			char [] space = new char [] {' '};
			
			string [] args;
			string s;
			while ((s = ndc.Reader.ReadLine ()) != null){
				Console.WriteLine ("=> " + s);
				if (s == String.Empty || s == "")
					continue;
				
				args = s.Split (space);
				if (CommandReceived != null)
					CommandReceived (s, args);
				Console.WriteLine ("Dispatching");
			}
		} catch (Exception e){
			Console.WriteLine ("Got an exception: " + e);
		}
	}

	public event NetCommand CommandReceived;

	public void Write (string s)
	{
		ndc.Writer.Write (s);
		ndc.Writer.Flush ();
	}
}
