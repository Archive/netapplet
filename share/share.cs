//
// Share interface user interface.
//
// 
// The program updates the contents if any new notifications are received from
// the daemon
//
// TODO:
//   Intense spinning box action while we get info from the daemon.
//   Add a cute timeout feature to let the user know if something goes wrong.
//   Actually call all the stuff Nat prepared.
//   Love the UI
//   Avoid resetting the interface list if new interfaces are added to the system.
//   
//
using Gtk;
using Glade;
using System;
using System.Collections;
using System.IO;

class InternetShare {
	public static void Main ()
	{
		Application.Init ();
		
		InternetShare s = new InternetShare ();

		Application.Run ();
	}

	[Widget] Window window;
	[Widget] TreeView treeview;
	[Widget] OptionMenu option_menu;
	[Widget] Button start;
	ListStore store;
	
	NetDaemon nd;
	ThreadNotify tn;

	public InternetShare ()
	{
		tn = new ThreadNotify (Reload);
		//
		// Setup the GUI
		//
		Glade.XML gui = new Glade.XML (null, "share.glade", "window", null);
		gui.Autoconnect (this);

		SetupTreeView ();
		
		//
		// Setup dummy values
		//
		option_menu.SetSizeRequest (180, -1);
		
		//
		// Get the data from the daemon.
		//
		nd = new NetDaemon ();
		nd.CommandReceived += ReplyHandler;
		nd.Write ("get_active");
		nd.Write ("list_interfaces");
		nd.Run ();
	}

	public void OnWindowDelete (object o, DeleteEventArgs args)
	{
		Application.Quit ();
	}

	void SetupTreeView ()
	{
		store = new ListStore (typeof (Interface));
		
		treeview.HeadersVisible = true;
		treeview.AppendColumn ("On", new CellRendererText (), new TreeCellDataFunc (CellDataActive));
		treeview.AppendColumn ("Ports", new CellRendererText (), new TreeCellDataFunc (CellDataPorts));

		treeview.Store = store;
	}

	void CellDataActive (TreeViewColumn tree_column, CellRenderer cell, TreeModel tree_model, TreeIter iter)
	{
		Interface iface = (Interface) store.GetValue (iter, 0);
		((CellRendererText) cell).Text = iface.Active.ToString ();
	}

	void CellDataPorts (TreeViewColumn tree_column, CellRenderer cell, TreeModel tree_model, TreeIter iter)
	{
		Interface iface = (Interface) store.GetValue (iter, 0);
		((CellRendererText) cell).Text = iface.Name;
	}

	//
	// Passed between ReplyHander and ThreadNotify
	//
	string cmd;
	string [] args;
	void ReplyHandler (string new_cmd, string [] new_args)
	{
		Console.WriteLine ("Here");
		lock (this){
			cmd = new_cmd;
			args = new_args;
			tn.WakeupMain ();
		}
	}

	//
	// Current interface list
	//
        Interface [] interfaces;

	//
	// Current active interface
	//
	string active;
	
	// A cache of operations.
	string last_active, last_interfaces;
	void Reload ()
	{
		Console.WriteLine ("There");
		lock (this){
			Console.WriteLine ("Got command: " + cmd);
			
			switch (args [0]){
			case "active":
				if (cmd == last_active)
					return;
				if (interfaces == null)
					continue;
				active = args [1];
				SelectActive ();
				break;
				
			case "interfaces":
				Console.WriteLine ("Here: cmd");
				if (cmd == last_interfaces)
					return;

				InterfacesAvailable (args);
				break;
			}
		}
	}

	//
	// Track the interfaces
	//    Populate the interfaces to share.
	//    Populate the list of client interfaces
	//
	public void InterfacesAvailable (string [] args)
	{
		Interface [] interfaces = new Interface [args.Length-1];
		int j = 0;
		int pos = -1;
		
		Menu m = new Menu ();
		for (int i = 1; i < args.Length; i+=2){
			interfaces [j] = new Interface (args [i]);
			string label = args [i] + " " + args [i+1];
			MenuItem mi = new MenuItem (label);
			m.Add (mi);
			if (active == args [i])
				pos = j;

			store.AppendValues (interfaces [j]);
			j++;

			// SETUP STORE
			
		}
		if (pos != -1)
			option_menu.SetHistory ((uint)pos);

		option_menu.Sensitive = true;
		m.ShowAll ();
		option_menu.Menu = m;
	}

	public void SelectActive ()
	{
		for (uint i = 0; i < interfaces.Length; i++){
			if (interfaces [i].Name == active){
				Console.WriteLine ("Select index {0}", i);
				option_menu.SetHistory (i);
				return;
			}
		}
	}
}

public class Interface {
	public string Name;
	public bool Active;
	
	public Interface (string name)
	{
		Name = name;
	}
}
