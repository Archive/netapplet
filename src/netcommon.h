#ifndef __NETCOMMON_H__
#define __NETCOMMON_H__

#include <glib.h>

#define NETDAEMON_SOCKET "/var/run/netapplet.socket"

#define TYPE_WIRELESS           "wireless"
#define TYPE_ETHERNET           "ethernet"
#define TYPE_DIALUP             "dialup"
#define TYPE_UNKNOWN            "unknown"

#define CLIPBOARD_NAME		"NETAPPLET_SELECTION"

const char * netcommon_verify_string (const char *str);

void netcommon_send_message (GIOChannel *channel,
			     const char *command,
			     ...);

void netcommon_watch_channel (GIOChannel *channel,
			      GHashTable *hash);

char *netcommon_escape_argument (const char *argument);

extern gboolean netapplet_get_clipboard (void);

#endif
