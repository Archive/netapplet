/*
 * src/netline.c - command-line client to the netdaemon
 *
 * Copyright (C) 2004 Novell, Inc.
 *
 * Licensed under the GNU GPL v2.  See COPYING.
 */

#define _GNU_SOURCE

#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <getopt.h>

#include "netcommon.h"

#define ERROR(fmt,arg...) fprintf(stderr,"%s/%d: " fmt,__FILE__,__LINE__,##arg)

int main (int argc, char *argv[])
{
	struct sockaddr_un addr;
	int fd, c;

	struct option longopts[] = {
		{ "change",		1, NULL, 'c' },
		{ "info",		1, NULL, 'g' },
		{ "list-ifaces",	0, NULL, 'i' },
		{ "get-active",		0, NULL, 'a' },
		{ NULL,			0, NULL, 0 }
	};

	fd = socket (PF_UNIX, SOCK_STREAM, 0);
	if (fd < 0) {
		ERROR ("Cannot create socket!");
		exit (EXIT_FAILURE);
	}

	memset (&addr, 0, sizeof (addr));
	addr.sun_family = AF_UNIX;
	snprintf (addr.sun_path, sizeof (addr.sun_path), NETDAEMON_SOCKET);

	if (connect (fd, (struct sockaddr *) &addr, sizeof (addr)) < 0) {
		ERROR ("Connect failed: %s\n", strerror (errno));
		exit (EXIT_FAILURE);
	}

	while ((c = getopt_long (argc, argv, "c:g:ia", longopts, NULL)) != -1) {
		char *buf;

		switch (c) {
		case 'c':
			buf = malloc (9 + strlen (optarg));
			sprintf (buf, "change %s\n", optarg);
			write (fd, buf, strlen (buf));
			free (buf);
			break;
		case 'i':
			buf = malloc (13);
			sprintf (buf, "list_ifaces\n");
			write (fd, buf, strlen (buf));
			free (buf);
			break;
		case 'g':
			buf = malloc (10 + strlen (optarg));
			sprintf (buf, "get_info %s\n", optarg);
			write (fd, buf, strlen (buf));
			free (buf);
			break;
		case 'a':
			buf = malloc (12);
			sprintf (buf, "get_active\n");
			write (fd, buf, strlen (buf));
			free (buf);
			break;
		}
	}

	close (fd);

	return 0;
}
